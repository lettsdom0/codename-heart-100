﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnClick : MonoBehaviour {

    public AudioClip ConfirmationSound;
    public AudioSource ConfirmationSoundSource;

    public void PlayConfirmOnClick()
    {
        ConfirmationSoundSource.Play();
    }

    // Use this for initialization
    void Start () {
        ConfirmationSoundSource.clip = ConfirmationSound;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

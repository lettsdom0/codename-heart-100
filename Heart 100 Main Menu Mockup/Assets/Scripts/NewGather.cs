﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGather : MonoBehaviour
{
    public GameObject I1;
    public GameObject I2;
    public GameObject I3;


    public int IngredientCounter;
    public int failsafeCounter;
    public int RandomCounter = 0;
    private int chargeFire = 7;

    public void RandomizeByNumber()
    {
        //when this runs, it will pick a random number spanning from 1 to 3, spawn the gameobject coreesponding to the number at one of the first 2 panels, and run again before breaking.
        var RandomIngredient = Random.Range(1, 4);
        if (RandomCounter < IngredientCounter && RandomCounter < 2)
        {
            if (RandomIngredient == 1)
            {

                if (RandomCounter == 1)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot1").transform;
                }
                else if (RandomCounter == 2)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot2").transform;
                }


            }
            else if (RandomIngredient == 2)
            {
                if (RandomCounter == 1)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot1").transform;
                }
                else if (RandomCounter == 2)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot2").transform;
                }

            }
            else if (RandomIngredient == 3)
            {
                if (RandomCounter == 1)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot1").transform;
                }
                else if (RandomCounter == 2)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot2").transform;
                }

            }



        }

        RandomCounter = RandomCounter + 1;
    }
    public void RandomizeByNumber2()
    {
        // when this function runs it will set an object to th 3rd or 4th panel, then run again before breaking
        var RandomIngredient = Random.Range(1, 4);
        if (RandomCounter < IngredientCounter && RandomCounter < 4)
        {
            if (RandomIngredient == 1)
            {

                if (RandomCounter == 3)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot3").transform;
                }
                else if (RandomCounter == 4)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot4").transform;
                }


            }
            else if (RandomIngredient == 2)
            {
                if (RandomCounter == 3)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot3").transform;
                }
                else if (RandomCounter == 4)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot4").transform;
                }

            }
            else if (RandomIngredient == 3)
            {
                if (RandomCounter == 3)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot3").transform;
                }
                else if (RandomCounter == 4)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot4").transform;
                }

            }
            RandomCounter = RandomCounter + 1;

        }
    }
    public void RandomizeByNumber3()
    {
        var RandomIngredient = Random.Range(1, 4);
        if (RandomCounter < IngredientCounter && RandomCounter < 7)
        {
            if (RandomIngredient == 1)
            {

                if (RandomCounter == 5)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot5").transform;
                }
                else if (RandomCounter == 6)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot6").transform;
                }

            }
            else if (RandomIngredient == 2)
            {
                if (RandomCounter == 5)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot5").transform;
                }
                else if (RandomCounter == 6)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot6").transform;
                }
            }
            else if (RandomIngredient == 3)
            {
                if (RandomCounter == 5)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot5").transform;
                }
                else if (RandomCounter == 6)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot6").transform;
                }
            }



            else if (RandomCounter >= IngredientCounter && RandomCounter <= 8)
            {
                if (RandomIngredient == 1)
                {
                    if (RandomCounter == 7)
                    {
                        GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                        go.transform.parent = GameObject.Find("Slot7").transform;
                    }

                    else if (RandomCounter >= 8)
                    {
                        print("too many");
                    }
                    else
                    {
                        print("ERROR");
                    }
                }
                else if (RandomIngredient == 2)
                {

                    if (RandomCounter == 7)
                    {
                        GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                        go.transform.parent = GameObject.Find("Slot7").transform;
                    }

                    else if (RandomCounter >= 8)
                    {
                        print("NU! IT MORE");
                    }
                    else
                    {
                        print("ERROR");
                    }
                }
                else if (RandomIngredient == 3)
                {
                    if (RandomCounter == 7)
                    {
                        GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                        go.transform.parent = GameObject.Find("Slot7").transform;
                    }

                    else if (RandomCounter >= 8)
                    {
                        print("NU! IT MORE");
                    }
                    else
                    {
                        print("ERROR");
                    }

                }

                RandomCounter = RandomCounter + 1;
            }
        }
    }
    private void CanIKeepGoing()
    {
        // tells the engine whether or not it can keep fireing charges.
        print("Okay " + failsafeCounter + " more times");
        if (chargeFire <= 7 && chargeFire > 5)
        {
            fire1();
            chargeFire = chargeFire - 1;
        }
        else if (chargeFire <= 5 && chargeFire > 3)
        {
            fire2();
            chargeFire = chargeFire - 1;
        }
        else if (chargeFire <= 3 && chargeFire > 0)
        {
            fire3();
            chargeFire = chargeFire - 1;
        }
        else
        {
            print("No. Stop it.");
        }
    }

    private void fire1()
    {
        RandomizeByNumber();
    }
    private void fire2()
    {
        RandomizeByNumber2();
    }
    private void fire3()
    {
        RandomizeByNumber3();
    }

    // Use this for initialization
    void Start()
    {
        print(failsafeCounter);
    }

    // Update is called once per frame
    void Update()
    {
        if (failsafeCounter < 8)
        {

            CanIKeepGoing();
            failsafeCounter = failsafeCounter + 1;

        }
        else
        {
            print("Don't keep going");
        }
    }
}

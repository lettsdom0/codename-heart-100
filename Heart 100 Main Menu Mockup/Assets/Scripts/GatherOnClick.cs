﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GatherOnClick : MonoBehaviour {
    public GameObject I1;
    public GameObject I2;
    public GameObject I3;
    

    public int IngredientCounter;
    public int failsafeCounter;
    public int RandomCounter = 0;
    private bool KeepGoing = false;

    public void RandomizeByNumber()
    {
        //when this runs, it will pick a random number spanning from 1 to 3, spawn the gameobject coreesponding to the number at one of the 5 panels, and run four more times before breaking.
        KeepGoing = true;
        var RandomIngredient = Random.Range(1, 4);
        if (RandomCounter < IngredientCounter)
        {
            if (RandomIngredient == 1)
            {
                
                if (RandomCounter == 1)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot1").transform;
                }
                else if (RandomCounter == 2)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot2").transform;
                }
                else if (RandomCounter == 3)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot3").transform;
                }
                else if (RandomCounter == 4)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot4").transform;
                }
                else if (RandomCounter == 5)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot5").transform;
                }
                else if (RandomCounter == 6)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot6").transform;
                }

            }
            else if (RandomIngredient == 2)
            {
                if (RandomCounter == 1)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot1").transform;
                }
                else if (RandomCounter == 2)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot2").transform;
                }
                else if (RandomCounter == 3)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot3").transform;
                }
                else if (RandomCounter == 4)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot4").transform;
                }
                else if (RandomCounter == 5)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot5").transform;
                }
                else if (RandomCounter == 6)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot6").transform;
                }
            }
            else if (RandomIngredient == 3)
            {
                if (RandomCounter == 1)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot1").transform;
                }
                else if (RandomCounter == 2)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot2").transform;
                }
                else if (RandomCounter == 3)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot3").transform;
                }
                else if (RandomCounter == 4)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot4").transform;
                }
                else if (RandomCounter == 5)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot5").transform;
                }
                else if (RandomCounter == 6)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot6").transform;
                }
            }

            RandomCounter = RandomCounter + 1;
        }
        else if (RandomCounter >= IngredientCounter)
        {
            if (RandomIngredient == 1)
            {
                if (RandomCounter == 7)
                {
                    GameObject go = Instantiate(I1, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot7").transform;
                }
                
                else if (RandomCounter >= 8)
                {
                    print("too many");
                }
                else
                {
                    print("ERROR");
                }
            }
            else if (RandomIngredient == 2)
            {

                if (RandomCounter == 7)
                {
                    GameObject go = Instantiate(I2, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot7").transform;
                }
                
                else if (RandomCounter >= 8)
                {
                    print("NU! IT MORE");
                }
                else
                {
                    print("ERROR");
                }
            }
            else if (RandomIngredient == 3)
            {
                if (RandomCounter == 7)
                {
                    GameObject go = Instantiate(I3, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.parent = GameObject.Find("Slot7").transform;
                }
 
                else if (RandomCounter >= 8)
                {
                    print("NU! IT MORE");
                }
                else
                {
                    print("ERROR");
                }
                
            }
            KeepGoing = false;
           

        }

       
    }
    private void CanIKeepGoing()
    {
        RandomizeByNumber();
    }

    // Use this for initialization
    void Start () {
        KeepGoing = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (KeepGoing == true)
        {
            if (failsafeCounter == 3)
            {
                print("Maximum Ingredients Reached");
                KeepGoing = false;
            }
            else
            {
                CanIKeepGoing();
                failsafeCounter = failsafeCounter + 1;
            }
        }
    }
}

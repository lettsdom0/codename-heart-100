﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags : MonoBehaviour {

    public const string chiliPepper = "Chili Pepper";
    public const string lavender = "Lavender";
    public const string snapDragon = "Snap Dragon";
    public const string bleedingHeart = "Bleeding Heart";
    public const string witchHazel = "Witch Hazel";
    public const string clover = "Clover";
    public const string mandrake = "Mandrake";
    public const string chamomile = "Chamomile";
    public const string bearberry = "Bearberry";
    public const string ginger = "Ginger";
    public const string skeletonFlower = "Skeleton Flower";
    public const string yarrow = "Yarrow";

}

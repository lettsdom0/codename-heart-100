﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : MonoBehaviour
{

    public int body = 1;
    public int mind = 0;
    public int spirit = 0;
    public int charisma = 0;

    int chili_pepper = 3;
    int lavender = 3;
    int snap_dragon = 3;
    int bleeding_heart = 2;
    int witch_hazel = 0;
    int clover = 1;
    int mandrake = 2;
    int chamomile = 1;
    int bearberry = 0;
    int ginger = 2;
    int skeleton_flower = 1;
    int yarrow = 0;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;

    void Start()
    {
        body = 1;
        mind = 0;
        spirit = 0;
        charisma = 0;
    }

    public void Mix()
    {
        //code checks what is above it and looks at their tags 
        //and then adds/subtracts from stats

        if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chiliPepper) {
            body += chili_pepper;
            charisma -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.lavender)
        {
            body += lavender;
            mind -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.snapDragon)
        {
            body += snap_dragon;
            spirit -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            body += bleeding_heart;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.witchHazel)
        {
            body += witch_hazel;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.clover)
        {
            body += clover;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.mandrake)
        {
            body += mandrake;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chamomile)
        {
            body += chamomile;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bearberry)
        {
            body += bearberry;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.ginger)
        {
            body += ginger;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            body += skeleton_flower;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.yarrow)
        {
            body += yarrow;
        }

        if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            body += chili_pepper;
            charisma -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.lavender)
        {
            body += lavender;
            mind -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.snapDragon)
        {
            body += snap_dragon;
            spirit -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            body += bleeding_heart;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.witchHazel)
        {
            body += witch_hazel;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.clover)
        {
            body += clover;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.mandrake)
        {
            body += mandrake;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chamomile)
        {
            body += chamomile;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bearberry)
        {
            body += bearberry;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.ginger)
        {
            body += ginger;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            body += skeleton_flower;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.yarrow)
        {
            body += yarrow;
        }

        if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            body += chili_pepper;
            charisma -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.lavender)
        {
            body += lavender;
            mind -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.snapDragon)
        {
            body += snap_dragon;
            spirit -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            body += bleeding_heart;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.witchHazel)
        {
            body += witch_hazel;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.clover)
        {
            body += clover;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.mandrake)
        {
            body += mandrake;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chamomile)
        {
            body += chamomile;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bearberry)
        {
            body += bearberry;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.ginger)
        {
            body += ginger;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            body += skeleton_flower;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.yarrow)
        {
            body += yarrow;
        }

        if (slot1.transform.childCount > 0) {
            Destroy(slot1.transform.GetChild(0).gameObject);
        }
        if (slot2.transform.childCount > 0)
        {
            Destroy(slot2.transform.GetChild(0).gameObject);
        }
        if (slot3.transform.childCount > 0)
        {
            Destroy(slot3.transform.GetChild(0).gameObject);
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Fungus;

public class Mix : MonoBehaviour {

    public GameObject bodyVat;
    public GameObject mindVat;
    public GameObject spiritVat;
    public GameObject charismaVat;

    public GameObject bodyTextGO;
    public GameObject mindTextGO;
    public GameObject spiritTextGO;
    public GameObject charismaTextGO;

    public GameObject bodyMindTextGO;
    public GameObject bodySpiritTextGO;
    public GameObject bodyCharismaTextGO;

    public GameObject mindBodyTextGO;
    public GameObject mindSpiritTextGO;
    public GameObject mindCharismaTextGO;

    public GameObject spiritBodyTextGO;
    public GameObject spiritMindTextGO;
    public GameObject spiritCharismaTextGO;

    public GameObject charismaBodyTextGO;
    public GameObject charismaMindTextGO;
    public GameObject charismaSpiritTextGO;

    TextMeshProUGUI bodyText;
    TextMeshProUGUI mindText;
    TextMeshProUGUI spiritText;
    TextMeshProUGUI charismaText;

    TextMeshProUGUI mindBodyText;
    TextMeshProUGUI mindSpiritText;
    TextMeshProUGUI mindCharismaText;

    TextMeshProUGUI bodyMindText;
    TextMeshProUGUI bodySpiritText;
    TextMeshProUGUI bodyCharismaText;

    TextMeshProUGUI spiritBodyText;
    TextMeshProUGUI spiritMindText;
    TextMeshProUGUI spiritCharismaText;

    TextMeshProUGUI charismaBodyText;
    TextMeshProUGUI charismaMindText;
    TextMeshProUGUI charismaSpiritText;

    Body body;
    Mind mind;
    Spirit spirit;
    Charisma charisma;

    public Flowchart flowchart;

    public int bodyBody = 0;
    public int bodyMind = 0;
    public int bodySpirit = 0;
    public int bodyCharisma = 0;

    public int mindBody = 0;
    public int mindMind = 0;
    public int mindSpirit = 0;
    public int mindCharisma = 0;

    public int spiritBody = 0;
    public int spiritMind = 0;
    public int spiritSpirit = 0;
    public int spiritCharisma = 0;

    public int charismaBody = 0;
    public int charismaMind = 0;
    public int charismaSpirit = 0;
    public int charismaCharisma = 0;

    bool mixed = false;

    private void Start()
    {
        body = bodyVat.GetComponent<Body>();
        mind = mindVat.GetComponent<Mind>();
        spirit = spiritVat.GetComponent<Spirit>();
        charisma = charismaVat.GetComponent<Charisma>();

        bodyText = bodyTextGO.GetComponent<TextMeshProUGUI>();
        mindText = mindTextGO.GetComponent<TextMeshProUGUI>();
        spiritText = spiritTextGO.GetComponent<TextMeshProUGUI>();
        charismaText = charismaTextGO.GetComponent<TextMeshProUGUI>();

        mindBodyText = mindBodyTextGO.GetComponent<TextMeshProUGUI>();
        mindSpiritText = mindSpiritTextGO.GetComponent<TextMeshProUGUI>();
        mindCharismaText = mindCharismaTextGO.GetComponent<TextMeshProUGUI>();

        bodyMindText = bodyMindTextGO.GetComponent<TextMeshProUGUI>();
        bodySpiritText = bodySpiritTextGO.GetComponent<TextMeshProUGUI>();
        bodyCharismaText = bodyCharismaTextGO.GetComponent<TextMeshProUGUI>();

        spiritBodyText = spiritBodyTextGO.GetComponent<TextMeshProUGUI>();
        spiritMindText = spiritMindTextGO.GetComponent<TextMeshProUGUI>();
        spiritCharismaText = spiritCharismaTextGO.GetComponent<TextMeshProUGUI>();

        charismaBodyText = charismaBodyTextGO.GetComponent<TextMeshProUGUI>();
        charismaMindText = charismaMindTextGO.GetComponent<TextMeshProUGUI>();
        charismaSpiritText = charismaSpiritTextGO.GetComponent<TextMeshProUGUI>();

        mixed = false;
    }

    public void Brew() {
        if (mixed == false) {
            body.Mix();
            mind.Mix();
            spirit.Mix();
            charisma.Mix();

            bodyBody = body.body;
            bodyMind = body.mind;
            bodySpirit = body.spirit;
            bodyCharisma = body.charisma;

            flowchart.SetIntegerVariable("Potion_Body", bodyBody);

            mindBody = mind.body;
            mindMind = mind.mind;
            mindSpirit = mind.spirit;
            mindCharisma = mind.charisma;

            flowchart.SetIntegerVariable("Potion_Mind", mindMind);

            spiritBody = spirit.body;
            spiritMind = spirit.mind;
            spiritSpirit = spirit.spirit;
            spiritCharisma = spirit.charisma;

            flowchart.SetIntegerVariable("Potion_Will", spiritSpirit);

            charismaBody = charisma.body;
            charismaMind = charisma.mind;
            charismaSpirit = charisma.spirit;
            charismaCharisma = charisma.charisma;

            flowchart.SetIntegerVariable("Potion_Charisma", charismaCharisma);

            bodyText.text = "Body: " + body.body;
            mindText.text = "Intellect: " + mind.mind;
            spiritText.text = "Spirit: " + spirit.spirit;
            charismaText.text = "Charisma: " + charisma.charisma;

            bodyMindText.text = "Intellect: " + body.mind;
            bodySpiritText.text = "Spirit: " + body.spirit;
            bodyCharismaText.text = "Charisma: " + body.charisma;

            mindBodyText.text = "Body: " + mind.body;
            mindSpiritText.text = "Spirit: " + mind.spirit;
            mindCharismaText.text = "Charisma: " + mind.charisma;

            spiritBodyText.text = "Body: " + spirit.body;
            spiritMindText.text = "Intellect: " + spirit.mind;
            spiritCharismaText.text = "Charisma: " + spirit.charisma;

            charismaBodyText.text = "Body: " + charisma.body;
            charismaMindText.text = "Intellect: " + charisma.mind;
            charismaSpiritText.text = "Spirit: " + charisma.spirit;

            flowchart.SetBooleanVariable("Done_Brewing", true);
            mixed = true;

            //TestDisplay();
        }
    }

    void TestDisplay() {
        print(bodyBody);
        print(bodyMind);
        print(bodySpirit);
        print(bodyCharisma);

        print(mindBody);
        print(mindMind);
        print(mindSpirit);
        print(mindCharisma);

        print(spiritBody);
        print(spiritMind);
        print(spiritSpirit);
        print(spiritCharisma);

        print(charismaBody);
        print(charismaMind);
        print(charismaSpirit);
        print(charismaCharisma);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spirit : MonoBehaviour
{

    public int spirit = 1;
    public int body = 0;
    public int mind = 0;
    public int charisma = 0;

    int chili_pepper = 1;
    int lavender = 2;
    int snap_dragon = 0;
    int bleeding_heart = 1;
    int witch_hazel = 2;
    int clover = 0;
    int mandrake = 3;
    int chamomile = 3;
    int bearberry = 3;
    int ginger = 1;
    int skeleton_flower = 0;
    int yarrow = 2;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;

    void Start()
    {
        spirit = 1;
        body = 0;
        mind = 0;
        charisma = 0;
    }

    public void Mix()
    {
        //code checks what is above it and looks at their tags 
        //and then adds/subtracts from stats
        if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            spirit += chili_pepper;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.lavender)
        {
            spirit += lavender;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.snapDragon)
        {
            spirit += snap_dragon;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            spirit += bleeding_heart;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.witchHazel)
        {
            spirit += witch_hazel;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.clover)
        {
            spirit += clover;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.mandrake)
        {
            spirit += mandrake;
            mind -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chamomile)
        {
            spirit += chamomile;
            charisma -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bearberry)
        {
            spirit += bearberry;
            body -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.ginger)
        {
            spirit += ginger;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            spirit += skeleton_flower;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.yarrow)
        {
            spirit += yarrow;
        }

        if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            spirit += chili_pepper;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.lavender)
        {
            spirit += lavender;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.snapDragon)
        {
            spirit += snap_dragon;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            spirit += bleeding_heart;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.witchHazel)
        {
            spirit += witch_hazel;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.clover)
        {
            spirit += clover;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.mandrake)
        {
            spirit += mandrake;
            mind -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chamomile)
        {
            spirit += chamomile;
            charisma -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bearberry)
        {
            spirit += bearberry;
            body -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.ginger)
        {
            spirit += ginger;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            spirit += skeleton_flower;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.yarrow)
        {
            spirit += yarrow;
        }

        if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            spirit += chili_pepper;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.lavender)
        {
            spirit += lavender;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.snapDragon)
        {
            spirit += snap_dragon;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            spirit += bleeding_heart;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.witchHazel)
        {
            spirit += witch_hazel;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.clover)
        {
            spirit += clover;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.mandrake)
        {
            spirit += mandrake;
            mind -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chamomile)
        {
            spirit += chamomile;
            charisma -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bearberry)
        {
            spirit += bearberry;
            body -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.ginger)
        {
            spirit += ginger;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            spirit += skeleton_flower;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.yarrow)
        {
            spirit += yarrow;
        }

        if (slot1.transform.childCount > 0)
        {
            Destroy(slot1.transform.GetChild(0).gameObject);
        }
        if (slot2.transform.childCount > 0)
        {
            Destroy(slot2.transform.GetChild(0).gameObject);
        }
        if (slot3.transform.childCount > 0)
        {
            Destroy(slot3.transform.GetChild(0).gameObject);
        }
    }

}

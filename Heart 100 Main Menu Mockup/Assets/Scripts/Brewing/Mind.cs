﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mind : MonoBehaviour {

    public int mind = 1;
    public int body = 0;
    public int spirit = 0;
    public int charisma = 0;

    int chili_pepper = 1;
    int lavender = 0;
    int snap_dragon = 1;
    int bleeding_heart = 3;
    int witch_hazel = 3;
    int clover = 3;
    int mandrake = 0;
    int chamomile = 2;
    int bearberry = 1;
    int ginger = 0;
    int skeleton_flower = 2;
    int yarrow = 1;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;

    void Start()
    {
        mind = 1;
        body = 0;
        spirit = 0;
        charisma = 0;
    }

    public void Mix() {
        //code checks what is above it and looks at their tags 
        //and then adds/subtracts from stats
        if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            mind += chili_pepper;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.lavender)
        {
            mind += lavender;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.snapDragon)
        {
            mind += snap_dragon;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            mind += bleeding_heart;
            charisma -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.witchHazel)
        {
            mind += witch_hazel;
            body -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.clover)
        {
            mind += clover;
            spirit -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.mandrake)
        {
            mind += mandrake;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chamomile)
        {
            mind += chamomile;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bearberry)
        {
            mind += bearberry;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.ginger)
        {
            mind += ginger;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            mind += skeleton_flower;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.yarrow)
        {
            mind += yarrow;
        }

        if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            mind += chili_pepper;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.lavender)
        {
            mind += lavender;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.snapDragon)
        {
            mind += snap_dragon;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            mind += bleeding_heart;
            charisma -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.witchHazel)
        {
            mind += witch_hazel;
            body -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.clover)
        {
            mind += clover;
            spirit -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.mandrake)
        {
            mind += mandrake;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chamomile)
        {
            mind += chamomile;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bearberry)
        {
            mind += bearberry;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.ginger)
        {
            mind += ginger;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            mind += skeleton_flower;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.yarrow)
        {
            mind += yarrow;
        }

        if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            mind += chili_pepper;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.lavender)
        {
            mind += lavender;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.snapDragon)
        {
            mind += snap_dragon;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            mind += bleeding_heart;
            charisma -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.witchHazel)
        {
            mind += witch_hazel;
            body -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.clover)
        {
            mind += clover;
            spirit -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.mandrake)
        {
            mind += mandrake;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chamomile)
        {
            mind += chamomile;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bearberry)
        {
            mind += bearberry;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.ginger)
        {
            mind += ginger;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            mind += skeleton_flower;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.yarrow)
        {
            mind += yarrow;
        }

        if (slot1.transform.childCount > 0)
        {
            Destroy(slot1.transform.GetChild(0).gameObject);
        }
        if (slot2.transform.childCount > 0)
        {
            Destroy(slot2.transform.GetChild(0).gameObject);
        }
        if (slot3.transform.childCount > 0)
        {
            Destroy(slot3.transform.GetChild(0).gameObject);
        }
    }

}

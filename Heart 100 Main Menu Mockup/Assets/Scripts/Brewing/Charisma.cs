﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charisma : MonoBehaviour
{

    public int charisma = 1;
    public int body = 0;
    public int mind = 0;
    public int spirit = 0;

    int chili_pepper = 0;
    int lavender = 1;
    int snap_dragon = 2;
    int bleeding_heart = 0;
    int witch_hazel = 1;
    int clover = 2;
    int mandrake = 1;
    int chamomile = 0;
    int bearberry = 2;
    int ginger = 3;
    int skeleton_flower = 3;
    int yarrow = 3;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;

    void Start()
    {
        charisma = 1;
        body = 0;
        mind = 0;
        spirit = 0;
    }

    public void Mix()
    {
        //code checks what is above it and looks at their tags 
        //and then adds/subtracts from stats
        if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            charisma += chili_pepper;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.lavender)
        {
            charisma += lavender;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.snapDragon)
        {
            charisma += snap_dragon;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            charisma += bleeding_heart;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.witchHazel)
        {
            charisma += witch_hazel;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.clover)
        {
            charisma += clover;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.mandrake)
        {
            charisma += mandrake;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.chamomile)
        {
            charisma += chamomile;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.bearberry)
        {
            charisma += bearberry;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.ginger)
        {
            charisma += ginger;
            mind -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            charisma += skeleton_flower;
            spirit -= 1;
        }
        else if (slot1.transform.childCount > 0 && slot1.transform.GetChild(0).tag == Tags.yarrow)
        {
            charisma += yarrow;
            body -= 1;
        }

        if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            charisma += chili_pepper;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.lavender)
        {
            charisma += lavender;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.snapDragon)
        {
            charisma += snap_dragon;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            charisma += bleeding_heart;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.witchHazel)
        {
            charisma += witch_hazel;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.clover)
        {
            charisma += clover;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.mandrake)
        {
            charisma += mandrake;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.chamomile)
        {
            charisma += chamomile;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.bearberry)
        {
            charisma += bearberry;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.ginger)
        {
            charisma += ginger;
            mind -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            charisma += skeleton_flower;
            spirit -= 1;
        }
        else if (slot2.transform.childCount > 0 && slot2.transform.GetChild(0).tag == Tags.yarrow)
        {
            charisma += yarrow;
            body -= 1;
        }

        if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chiliPepper)
        {
            charisma += chili_pepper;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.lavender)
        {
            charisma += lavender;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.snapDragon)
        {
            charisma += snap_dragon;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bleedingHeart)
        {
            charisma += bleeding_heart;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.witchHazel)
        {
            charisma += witch_hazel;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.clover)
        {
            charisma += clover;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.mandrake)
        {
            charisma += mandrake;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.chamomile)
        {
            charisma += chamomile;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.bearberry)
        {
            charisma += bearberry;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.ginger)
        {
            charisma += ginger;
            mind -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.skeletonFlower)
        {
            charisma += skeleton_flower;
            spirit -= 1;
        }
        else if (slot3.transform.childCount > 0 && slot3.transform.GetChild(0).tag == Tags.yarrow)
        {
            charisma += yarrow;
            body -= 1;
        }

        if (slot1.transform.childCount > 0)
        {
            Destroy(slot1.transform.GetChild(0).gameObject);
        }
        if (slot2.transform.childCount > 0)
        {
            Destroy(slot2.transform.GetChild(0).gameObject);
        }
        if (slot3.transform.childCount > 0)
        {
            Destroy(slot3.transform.GetChild(0).gameObject);
        }
    }

}

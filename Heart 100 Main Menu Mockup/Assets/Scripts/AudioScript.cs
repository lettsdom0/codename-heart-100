﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {

    public AudioClip SceneAudio;
    public AudioClip SceneMusic;
    public AudioSource SceneMusicSource;
    public AudioSource SceneAudioSource;
    
    public void PlayMe()
    {
        SceneMusicSource.Play();
    }

    // Use this for initialization
    void Start () {
        SceneMusicSource.clip = SceneMusic;
        SceneAudioSource.clip = SceneAudio;
        PlayMe();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            SceneAudioSource.Play();
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            SceneAudioSource.Play();
    }
}
